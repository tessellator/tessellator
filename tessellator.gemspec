# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tessellator/version'

Gem::Specification.new do |spec|
  spec.name          = "tessellator"
  spec.version       = Tessellator::VERSION
  spec.authors       = ["Ellen Dash"]
  spec.email         = ["me@duckie.co"]
  spec.summary       = %q{A web browser written in Ruby.}
  spec.homepage      = "https://gitlab.com/tessellator/tessellator"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "tessellator-fetcher", "~> 6.0"

  spec.add_runtime_dependency "cairo", "~> 1.15.9"
  spec.add_runtime_dependency "cairo-gobject", "~> 3.1.8"
  spec.add_runtime_dependency "pango", "~> 3.1.8"
  spec.add_runtime_dependency "nokogiri"
  spec.add_runtime_dependency "mime-types"
  spec.add_runtime_dependency "openssl-better_defaults", "~> 0.0.1"

  spec.add_runtime_dependency "wot-utilities", "~> 1.0"
  spec.add_runtime_dependency "default", "~> 1.0"

  spec.add_runtime_dependency "gtk3",  "~> 3.1.8"

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 12.0"
  spec.add_development_dependency "minitest"
end
