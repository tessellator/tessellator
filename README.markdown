# Tessellator

Web browser in Ruby.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tessellator'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tessellator

## Usage

TODO: Write usage instructions here

## Screenshots

![Screenshot](https://gitlab.com/duckinator/tessellator/raw/main/assets/screenshot.png)

## Contributing

1. Fork it ( https://gitlab.com/duckinator/tessellator/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request
